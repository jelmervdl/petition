<?php
header('Content-Type: text/html; charset=utf-8');

require_once 'config.php';

if (!empty($_POST['name'])
	&& !empty($_POST['email'])
	&& !empty($_POST['place'])
	&& !empty($_POST['relation'])
	&& !empty($_POST['story']))
{
	$stmt = $pdo->prepare("INSERT INTO signatures(name, email, place, relation, story, signed_on) VALUES(?, ?, ?, ?, ?, NOW())");

	$stmt->execute(array(
		trim($_POST['name']),
		trim($_POST['email']),
		trim($_POST['place']),
		trim($_POST['relation']),
		trim($_POST['story'])
	));

	header('Location: index.php?thanks=true#thank-you');
	exit;
}

$sigcount = $pdo->query("SELECT COUNT(id) FROM signatures WHERE public = 1");

$signatures = $pdo->query("SELECT id, name, place, relation, story, signed_on FROM signatures WHERE public = 1 ORDER BY signed_on DESC");
$signatures->setFetchMode(PDO::FETCH_OBJ);

function html_encode($text) {
	return htmlspecialchars($text, ENT_COMPAT, 'UTF-8');
}

function format_story($text)
{
	$paragraphs = array();

	$text = str_replace("\r", "", $text);

	foreach (explode("\n\n", $text) as $paragraph)
		$paragraphs[] = sprintf('<p>%s</p>', format_paragraph($paragraph));

	return implode("\n", $paragraphs);
}

function format_paragraph($text)
{
	return htmlspecialchars(trim($text));

	// return str_replace("\n", '<br>', $html);
}

?>
<!DOCTYPE html>
<html>
	<head>
		<title>Petition &ndash; Cover</title>
		<link rel="stylesheet" href="default.css">
		<meta name="robots" content="noindex, nofollow">
	</head>
	<body>
		<header>
			<a href="index.php"><img class="logo" src="https://sd.svcover.nl/Logos/vereniging/cover_logo.png"></a>
			<h1>Petition</h1>
		</header>

		<div class="content">
			<section id="issue">
				<h2>Issue</h2>
				<p>The Education Support Centre (ESC) has an urgent need for office space. An equivalent of 15 full time jobs has been hired, but all offices in the ESC are either occupied or unsuitable for workplaces. Therefore, the faculty board is searching for office space close to the ESC and using the rooms on the ground floor seem to be the most suitable option.</p>
				<p>As for now, it is not certain whether the Cover room itself is needed too, but it is almost certain that the rooms adjacent to the Cover room will become part of the ESC. The open space in front of the Cover room will have to change to a more office like environment, and in case the Cover room is not needed for the ESC, the character of the Cover room has to change from a social space to an office space as well.</p>
				<p>We will most likely lose the Cover room in its current form.</p>
			</section>

			<section id="letter">
				<h2>Letter to the Faculty Board</h2>
				<p>To defend our situation, we have written a short letter to the faculty board in which we explain why Cover is important for both the students and departments of AI and CS. We hope this letter will be signed by a large number of students and staff.</p>
				<p class="plea"><em>We ask you to co-sign our letter to show that you support this view. If possible, please sign the letter in the Cover room.</em></p>
				<p>You have <em>till Monday 10th of Februari</em> to co-sign the letter. On tuesday we will submit it. After tuesday you can still show your support by <a href="#petition">signing the petition</a> on this web page.</p>
				<blockquote cite="https://sd.svcover.nl/Documenten/Letter%20Faculty%20Board%20Cover.pdf">
					<p>Dear Faculty Board,</p>
					<p>We, the undersigned, write you this letter because Cover, the study association for Artificial Intelligence and Computing Science, is threatened to lose its room on the ground floor of the Bernoulliborg due to the need for expansion of the Education Support Centre. Losing this place and atmosphere would be devastating to Cover.</p>
					<p>Cover plays an important role in the self-education of many Artificial Intelligence and Computing Science students. It does not only organise and involve students in study related activities but through social activities it keeps students active and involved with their study environment and staff.</p>
					<p>A place to lounge, blow off steam and chat with fellow bachelor and master students is an essential part of its contribution to their study environment.</p>
					<p>To fulfill its role Cover needs to stay close to its students and it needs to have a place for them in the Bernoulliborg. At this moment the location of Cover’s room is ideal. Moving Cover to a less suitable place would cripple this social feature for students and will have a considerable negative impact on the atmosphere of Artificial Intelligence and Computing Science at the University of Groningen.</p>
					<p>We are prepared to contribute in finding a solution for the situation regarding the Education Support Centre, but we want to firmly state that any solution that impedes Cover in its task is undesirable for both staff and students.</p>
					<p>Yours faithfully,</p>
				</blockquote>
			</section>

			<section id="petition">
				<form method="post" action="index.php">
					<label for="name">Name:</label>
					<input type="text" id="name" name="name" placeholder="Your name" maxlength="255">

					<label for="email">Email address: <small>(Will not be publicly visible)</small></label>
					<input type="email" id="email" name="email" placeholder="Your email" maxlength="255">

					<label for="place">Place of residence:</label>
					<input type="text" id="place" name="place" placeholder="e.g. Groningen" maxlength="255">

					<label for="relation">Relation to <abbr title="Artificial Intelligence">AI</abbr> and <abbr title="Computer Science">CS</abbr> at the University of Groningen:</label>
					<input type="text" id="relation" name="relation" placeholder="e.g. former student CS" maxlength="255">

					<label for="story">Why should Cover keep a place like the SLACK:</label>
					<textarea name="story" id="story" rows="8" placeholder="How did Cover contribute to your study environment?"></textarea>

					<button type="submit">Sign the petition</button>
					<span class="call-to-action">Join the <em><?=$sigcount->fetchColumn()?></em> people who signed before you.</span>
				</form>
			</section>

			<?php if (!empty($_GET['thanks'])): ?>
			<section id="thank-you">
				<h2>Thank you.</h2>
				<p>Thank you on behalf of the board of Cover. We really value all your support.</p>
				<p>Please share this page with your fellow alumni, colleges and students. Ask them if they would miss or would have missed Cover and if they do, let them know that they have to sign before tuesday.</p>
				<p><a href="https://www.facebook.com/sharer/sharer.php?u=<?=rawurlencode('http://petition.svcover.nl/')?>" class="share-link share-on-facebook" target="_blank">Share on Facebook</a><a href="https://twitter.com/share?url=<?=rawurlencode('http://petition.svcover.nl/')?>&amp;text=<?=rawurlencode('Help to keep Cover a social place for students and sign this petition:')?>" class="share-link share-on-twitter" target="_blank">Share on Twitter</a></p>
			</section>
			<?php endif ?>

			<section id="stories" class="stories">
				<?php foreach ($signatures as $signature): ?>
				<article class="story" id="<?=$signature->id?>">
					<?php $timestamp = DateTime::createFromFormat('Y-m-d H:i:s', $signature->signed_on); ?>
					<blockquote>
						<?=format_story($signature->story)?>
					</blockquote>
					<p class="author">
						<span class="name"><?=html_encode($signature->name)?></span>,
						<span class="relation"><?=html_encode($signature->relation)?></span>,
						<span class="place"><?=html_encode($signature->place)?></span>,
						<time datetime="<?=$timestamp->format(DateTime::RFC3339)?>" class="signed_on"><?=$timestamp->format('j-n-Y') ?></time>.
						<a class="permalink" href="#<?=$signature->id?>">#<?=$signature->id?></a>
					</p>
				</article>
				<?php endforeach ?>
			</section>

			<footer>
				<p>For information please contact bestuur@svcover.nl.</p>
			</footer>
		</div>
	</body>
</html>